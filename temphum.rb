#!/home/pi/.rvm/rubies/ruby-2.1.0/bin/ruby
require 'thingspeak'
require 'yaml'

module Enumerable
  def sum
    self.inject(0){|accum, i| accum + i }
  end

  def mean
    self.sum/self.length.to_f
  end

  def sample_variance
    m = self.mean
    sum = self.inject(0){|accum, i| accum +(i-m)**2 }
    (1/self.length.to_f*sum)
  end

  def standard_deviation
    return Math.sqrt(self.sample_variance)
  end
end 

config = YAML.load_file('/home/pi/temphum/config.yml')

write_key = config["config"]["write_key"]
read_key = config["config"]["read_key"]
pin = config["config"]["pin"]

client = ThingSpeak::Client.new(write_key, read_key)

temp_array = []
humidity_array = []

15.times do
  data = `sudo /home/pi/temphum/Adafruit_DHT 2302 #{pin}`
  temp, humidity = data.split("\n").last.scan(/[-+]?([0-9]*\.[0-9]+|[0-9]+)/).flatten
  if temp.to_f > 0 && humidity.to_f > 0
    temp_array << temp.to_f
    humidity_array << humidity.to_f
    #temp_f = (9.0 / 5.0 * temp.to_f) + 32
    #client.update_channel({field1: temp, field2: humidity, field3: "%2.2f" % temp_f})
  end
  sleep(3)
end

def filter_array(array)
  return array.delete_if{|reading| (array.mean - reading) > (2*array.standard_deviation)}.mean.to_f
end

average_temp = filter_array(temp_array)
average_humidity = filter_array(humidity_array)
average_temp_f = (9.0 / 5.0 * average_temp) + 32
unless average_temp.nan? || average_humidity.nan?
  client.update_channel({field1: "%2.2f" % average_temp, field2: "%2.2f" % average_humidity, field3: "%2.2f" % average_temp_f})
  # puts "average temp: #{'%2.2f' % average_temp}, average humidity: #{'%2.2f' % average_humidity}"
end
